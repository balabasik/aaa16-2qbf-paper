Given a non CNF formula $\phi$ (e.g., represented as an AIG) 
one could \emph{CNFize} it (i.e., transform it to an equisatisfiable CNF form,
for example by using Tseitin transform~\cite{Tse70}) to get $\phi_{CNF}$, and then
run the \textsc{Qesto} algorithm introduced in Section 3. 
We, however, speculate that this is an inefficient approach. The following
example shows that solving a 2QBF formula with its original matrix
$\phi$ could be more effective compared to CNF-based solving.
%%cs Actually "much more" is not justified by this example.
%%cs Finding another example where the benefit is more clear would by good, 
%%cs but will take time.
%
\begin{example}\label{ex:circuit_cof}
Consider the following simple true 2QBF formula $\Phi = \forall x
\exists ab . \phi$, where $\phi$ is given as a circuit 
\[
(g_1 = \text{AND}(x,a)) \ (g_2 = \text{AND}(\overline x,b)) \ (f = \text{OR}(g_1,g_2)),
\] 
with a single output $f$ and two internal
gates $g_1$ and $g_2$. Assume now that the synthesis solver in
algorithm \texttt{Cegar2Qbf} depicted in Figure~\ref{alg:cegar} 
comes up with a candidate
$\alpha_X=\overline x$ (i.e., assigns $x=0$), and the verification
solver returns a counterexample $\alpha_Y=ab$ (i.e., assigns
$a=1$ and $b=1$). Note that in this case $\texttt{negCof}=0$,
i.e., it blocks all the universal candidate assignments, and we
immediately conclude that $\Phi$ is true.

Now let us look at the same 2QBF, but CNFized by Tseitin transform prior to solving,
i.e., at
%
\begin{eqnarray*}
\Phi' &=& \forall x \exists abg_1g_2 . \phi_{CNF}, \text{ where}\\
\phi_{CNF}&=&(g_1+\overline x+\overline a)(\overline{g}_1+x)(\overline{g}_1+a)\\
&\wedge&(g_2+x+\overline b)(\overline{g}_2+\overline
x)(\overline{g}_2+b)(g_1+g_2).
\end{eqnarray*}
%
Similarly, assume that the synthesis solver in algorithm
\texttt{Cegar2Qbf} guesses candidate $\alpha_X=\overline x$.
The verification solver now replies with a counterexample
$\alpha_Y=ab\overline{g}_1g_2$. \texttt{negCof} in this case is
computed to be $\texttt{negCof}=x$, i.e., it only blocks the
candidate $x=0$, and one more iteration is needed to conclude that
$\Phi'$ is true.
\end{example}

The intuition behind Example~\ref{ex:circuit_cof} is as follows. 
Whenever the counterexample (line 5 in Figure~\ref{alg:cegar}) 
is computed on CNF level, it fixes a value assignment to the 
auxiliary variables (i.e., intermediate variables introduced during CNFization) as well.
The negated cofactor (line 7 in Figure~\ref{alg:cegar}) in this case 
shall only block $X$ assignments which respect those values.
This phenomenon is summarized in Proposition~\ref{prop:circuit}.

\begin{proposition}\label{prop:circuit}
Given a 2QBF $\Phi = \forall X \exists Y . \phi$, with $\phi$ represented as a circuit, 
and an assignment $\alpha_Y$ to $Y$ variables. Further given two assignments 
$\alpha_{X_1}$ and $\alpha_{X_2}$ to $X$ variables, such that 
\[
\phi|_{\alpha_Y\alpha_{X_1}} \ \wedge \ \phi|_{\alpha_Y\alpha_{X_2}} \ \wedge \
\exists G\in\phi:G|_{\alpha_Y\alpha_{X_1}}\oplus G|_{\alpha_Y\alpha_{X_2}}
\] 
(i.e., the output of $\phi$ evaluates to the same value under two input assignments, 
but there is an internal gate disagreement). 
Then $\alpha_{X_1}$ and $\alpha_{X_2}$ shall be blocked within 
the \emph{same} iteration 
computing counterexample $\alpha_Y$ in line 5 of Figure~\ref{alg:cegar},
if algorithm \texttt{Cegar2Qbf} is applied to $\Phi$.
On the other hand $\alpha_{X_1}$ and $\alpha_{X_2}$ \emph{shall not} 
be blocked within the same iteration, if \texttt{Cegar2Qbf} is applied 
to a (by Tseitin transform) CNFized version of $\Phi$.
\end{proposition}

Example~\ref{ex:circuit_cof} and Proposition~\ref{prop:circuit}
show that flattening the circuit
structure into CNF affects the 2QBF solving process more than it does
for propositional SAT. SAT solvers use CNF structure for
efficiency reasons, e.g., for clause learning. In CEGAR based QBF
solvers, however, the situation is different. 
As experiments shall confirm later,
one may efficiently use CNF for underlying SAT queries, but 
cofactoring on circuit level 
instead of CNF decreases the number of iterations needed 
for completion of algorithm \texttt{Cegar2Qbf} a lot.
We shall see that this decrease in the number of iterations even overcomes 
the benefits of efficient cofactor representation 
in \textsc{Qesto} algorithm applied after circuit CNFization.

%%cs I did not understand the following sentence.
%%cs Why *three* level, not tow-level? Is this sentence needed anyway?
%%cs
%%cs As CNF can be viewed as a three level circuit, 
%%cs one intriguing question is whether circuit based 2QBF solvers 
%%cs can offer an efficient cofactor representation to 
%%cs compete with the \textsc{Qesto} approach on CNF problems.
Please note that, even in circuit 2QBF solvers, the SAT queries in line 3 of algorithm Cegar2Qqbf 
(Figure~\ref{alg:cegar}) are 
made to a CNF-based SAT solver. 
This choice is caused by a specific ``incremental'' nature 
of the underlying SAT calls: Please recall that synthesis manager \texttt{synMan} is updated by iterative 
conjunction with \texttt{negCof} in line 8 of Figure~\ref{alg:cegar}, 
i.e., in each iteration clauses from the CNFized
\texttt{negCof} are added to manager \texttt{synMan}.
%
Following the ideas of \emph{structural hashing} of nodes 
in And-Inverter graphs (AIGs) we propose
algorithm \texttt{AigShare2Qbf} depicted in Figure~\ref{alg:cof_share} 
for circuit 2QBF solving. 

\begin{figure}[t]
\center{AigShare2Qbf}\\
\vspace{-2mm} \hrulefill
\begin{program}
\INPUT:  a QBF $\Phi = \forall X \exists Y . \phi$\\
\OUTPUT: True or False\\
\BEGIN\\
01 \> \> $\texttt{synMan}[X]:=1$; $\texttt{verMan}[X,Y]:=\phi; \texttt{aigMan}:=\emptyset$;\\
02 \> \> \WHILE True\\
03 \> \> \> $\alpha_X$ := SatSolve($\texttt{synMan}$);\\
04 \> \> \> \IF $\alpha_X=\emptyset$ \THEN \RETURN True;\\
05 \> \> \> $\alpha_Y$ := SatSolve($\texttt{verMan}$, $\alpha_X$);\\
06 \> \> \> \IF $\alpha_Y=\emptyset$ \THEN \RETURN False;\\
07 \> \> \> $\texttt{negCof}$ := AIG$(\neg \phi|_{\alpha_Y})$;\\
08 \> \> \> $\texttt{newAnd}$ := NotHashed(\texttt{negCof},\texttt{aigMan});\\
09 \> \> \> Hash$(\texttt{aigMan},\texttt{newAnd})$;\\
10 \> \> \> $\texttt{synMan}$ := $\texttt{synMan}\wedge\text{CNF}(\texttt{newAnd})$;\\
\END
\end{program}
\vspace{-2mm} \hrulefill \vspace{-2.5mm} 
\caption{\small CEGAR algorithm for AIG based 2QBF 
solving with cofactor sharing heuristics.} \label{alg:cof_share}
\end{figure}
\renewcommand\setstretch{1}

The core CEGAR procedure of algorithm \texttt{AigShare2Qbf} is the same 
as that of \texttt{Cegar2Qbf}. Furthermore \texttt{Cegar2Qbf} 
may use AIGs as an underlying structure of its verification manager 
and negated cofactors as well. The only difference is 
the \emph{cofactor hashing} heuristics 
(lines 8 and 9 in Figure~\ref{alg:cof_share}).
More specifically in line 8 we extract a subset \texttt{newAnd}
of AND gates from $\texttt{negCof}$ that have not been 
hashed previously. Then in line 9 we hash them and add them
to the AIG manager \texttt{aigMan}. In line 10 we 
add CNFized \texttt{newAnd} gates to \texttt{synMan}.
For AND gates in line 8 which have been hashed previously,
\texttt{synMan} already contains clauses for the corresponding definitions.
As shall be seen from experiments, cofactor sharing
heuristic gives a significant improvement on 
the benchmarks where a lot of iterations are needed for algorithm 
\texttt{Cegar2Qbf} to converge. 
%
%The last thing to mention is the connection between the cofactor sharing 
%heuristics and \textsc{Qesto}. We speculate that under a careful implementation 
%of cofactor sharing the size of \texttt{aigMan} should be bound by the 
%number of clauses in the original CNF formula, just as it is in \textsc{Qesto}.
%In a sense \textsc{Qesto} approach guesses ``the best possible'' CNFization
%of negated cofactors, through the circuit nodes representing definitions 
%of each clause in the matrix CNF.


Given the extra-strength of circuit-based 2QBF solvers, in this
section we introduce a procedure (referred to as \emph{de-CNFization}) 
of converting a plain CNF formula
into an equi\-satisfiable single-output circuit. 
\emph{De-CNFization} was already partially used in SAT and QBF solving,
by extracting gate definitions from CNF~\cite{ogms:2002,eb:2005,PS:2009,GB:2013}. 
Earlier approaches relied on a syntactic extraction of gate definitions, i.e.,
they looked for subsets of a CNF corresponding to gate definitions in a certain
form. This approach has two basic disadvantages: First, the approach is only able to detect
gate definitions for pre-defined gate types (typically ``AND-like'' and ``XOR-like'' gates),
other gate definitions can not be found. Secondly, the CNF for the definition of a non-trivial
gate does not need to be unique. In such a case, syntactic approaches looking for certain patterns
will have difficulties. In this section we propose an efficient gate extraction algorithm relying on
semantic rather than syntactic gate detection.

An overview of our de-CNFization algorithm is given in
Figure~\ref{alg:circuit}. We use standard ``template-matching''
ways to find AND-like gates (line 1 in Figure~\ref{alg:circuit},
function \textit{FindAnd}) and XOR-like gates (line 3 in
Figure~\ref{alg:circuit}, function \textit{FindXor}). After that we make use of
the more general ``semantic-based'' definition detection (line 5 in
Figure~\ref{alg:circuit}, function \textit{FindSem}).
%Template-based functions FindAnd and FindXor simply iterate over
%variables and match their occurrences with predefined templates.
%Assuming that the maximal clause length is fixed and that the number of variable
%occurrences is bounded by a constant, both functions take
%worst case linear time complexity with respect to the number of variables in the CNF.
%
\begin{figure}[t]
\center{CnfToCircuit}\\
\vspace{-2mm} \hrulefill
\begin{program}
\INPUT:  a CNF $\phi_{CNF} = \{C_1,\ldots ,C_n\}$\\
\OUTPUT: circuit $\phi$\\
\BEGIN\\
01 \> \> $\texttt{andGates}$ := FindAnd($\phi_{CNF}$); \\
02 \> \> $\texttt{restCls}$  := $\phi_{CNF}\setminus \text{Cls}(\texttt{andGates})$ ;\\
03 \> \> $\texttt{xorGates}$ := FindXor($\texttt{restCls}$);\\
04 \> \> $\texttt{restCls}$  := $\texttt{restCls}\setminus \text{Cls}(\texttt{xorGates})$ ;\\
05 \> \> $\texttt{semGates}$ := FindSem($\texttt{restCls}$);\\
06 \> \> $\texttt{restCls}$  := $\texttt{restCls}\setminus \text{Cls}(\texttt{semGates})$ ;\\
07 \> \> $\texttt{allGates}$ := $\texttt{andGates}\cup \texttt{xorGates} \cup\texttt{semGates}$ ; \\
08 \> \> $\texttt{PIs}$      := UndefinedVars(\texttt{allGates});\\
09 \> \> \FOREACH $C_i$ in $\texttt{restCls}$\\
10 \> \> \> $\texttt{allGates}$ := $\texttt{allGates}\cup (g_i = C_i)$; \\
11 \> \> $\texttt{allGates}$ := $\texttt{allGates}\cup (\texttt{PO}=\bigwedge(g_i))$; \\
12 \> \> $\phi$ := BuildCircuit($\texttt{PIs}$,$\texttt{PO}$,$\texttt{allGates}$);\\
\END
\end{program}
\vspace{-2mm} \hrulefill \vspace{-2.5mm} 
\caption{\small De-CNFization procedure.} \label{alg:circuit}
\end{figure}
\renewcommand\setstretch{1}

To see how the \texttt{CnfToCircuit} algorithm works consider the following CNF:
\begin{example}
\label{extract_example}
\begin{eqnarray*}
\phi_{CNF} &    =   & (\overline x+\overline a+c)(x+\overline{c})(a+\overline{c}) \\
     & \wedge & (b+d+x)(b+\overline d+\overline x)(\overline b+d+\overline y)(\overline b+\overline d+y) \\
     & \wedge & (\overline f + c + d)(f + \overline c)(f + \overline d)f
\end{eqnarray*}
%
Note that it encodes the satisfiability problem of the combinational circuit
\[
\phi = [c = \text{AND}(a,x)] \wedge [d = \text{ITE}(b,y,\overline
x)] \wedge [f = \text{OR}(c,d)],
\]
where $a$, $b$, $x$, $y$ are the primary inputs of the circuit,
$c$ and $d$ are internal gates, and $f$ is its single primary
output. Therefore the satisfiability of $\phi_{CNF}$ could be
determined by the satisfiability of $\phi$. 
\end{example} 
%

Note that \textit{FindAnd} routine of algorithm \texttt{CnfToCircuit} 
shall detect gate definitions for variables $c$ and $f$,
but will fail to detect a gate definition for the variable $d$.
The definition for variable $d$ will be found using semantic extraction
routine \textit{FindSem}, as is explained below.

Intuition suggests the following informal condition for the presence
of a gate definition: The \emph{gate} definition for variable $x$ 
is encoded in CNF, 
if under any assignment to other variables a \emph{unique} assignment to $x$
satisfies the formula. 
If we relax the \emph{unique} condition to \emph{at most one},
then we say that $x$ is a \emph{dependent} variable
(the difference shall be seen later).
In both cases $x$ could be re-expressed as a function of other variables, 
without changing the satisfiability of the formula.

We first start with a general method for semantic gate extraction which
is then followed by an efficient heuristics.
Let us consider a CNF $\phi_{CNF}$ which is partitioned into two 
disjoint
sets $\phi_{def}$ and $\phi_{rem}$. The subset $\phi_{def}$ contributes a 
gate definition for a variable $x$, if $\phi_{def}|_{\overline{x}} \oplus \phi_{def}|_{x}$
is valid (or equivalently if $\phi_{def}|_{\overline{x}} \equiv \phi_{def}|_{x}$ is unsatisfiable).
Then $x = \overline{\phi_{def}}|_{\overline{x}}$ 
(or equivalently $x = \phi_{def}|_{x}$) is a suitable definition for variable $x$
as the following theorem states.

\begin{theorem}\label{th:general}
Consider a CNF $\phi_1 = \phi_{def} \wedge \phi_{rem}$
and the (non CNF) formula
$\phi_2 = (x \equiv \overline{\phi_{def}}|_{\overline{x}}) \wedge \phi_{rem}$,
%$\phi_3 = (x \equiv \phi_{def}|_{x}) \wedge \phi_{rem}$.
Then $\phi_1$ and $\phi_2$ 
%($\phi_3$) 
are equivalent, if
$\phi_{def}|_{\overline{x}} \equiv \phi_{def}|_{x}$ is unsatisfiable.
\end{theorem}
%
\begin{proof}
%We only consider equivalence of $\phi_1$ and $\phi_2$.
%The case of $\phi_3$ is analogous.
It is easy to see that using the precondition of the theorem,
$\phi_1$ can be rewritten into $\phi_2$.
It holds 
\begin{eqnarray*}
\phi_1 & = & \phi_{def} \wedge \phi_{rem} \\
       & = & (x + \phi_{def}|_{\overline{x}}) \wedge (\overline{x} + \phi_{def}|_x) \wedge \phi_{rem} \\
       & = & (x + \phi_{def}|_{\overline{x}}) \wedge (\overline{x} + \overline{\phi_{def}}|_{\overline{x}}) \wedge \phi_{rem} \\
       &   & \mbox{(since } \phi_{def}|_{x} \equiv \overline{\phi_{def}}|_{\overline{x}} \mbox{  due to precondition)} \\
       & = & (x \equiv \overline{\phi_{def}}|_{\overline{x}}) \wedge \phi_{rem} \; = \; \phi_2.\\[-8mm]
\end{eqnarray*}
\end{proof}

The semantic gate detection according to Theorem~\ref{th:general} has the disadvantage
that the partition of the CNF $\phi_1$ into $\phi_{def}$ and $\phi_{rem}$ has to be
guessed. This problem can be solved in principle by introducing a set $D$ of new de-activation variables $d_i$ into the problem.
From $\phi_1(Y)$ we compute a CNF $\phi'_1(D, Y)$ by replacing each clause $C_i$ in $\phi_1$ by $C_i + d_i$, i.e., the clause may be
de-activated by setting $d_i = 1$. Now each assignment to the de-activation variables in some satisfying solution to the 2QBF
$\exists D \forall Y \phi'_1|_{\overline{x}} \oplus \phi'_1|_{x}$
defines an appropriate subset $\phi_{def}$ of $\phi_1$ according to Theorem~\ref{th:general}.

To improve the performance of the introduced approach we propose to use two heuristics. 
First, we limit the search for the gate definitions 
of variable $x$ only to clauses that contain $x$ or $\overline{x}$.
%(all other clauses are de-activated anyway).
%The efficiency of this heuristics highly depends on the 
%CNFization technique (if any) that was used to obtain the CNF.
Note that for the commonly used Tseitin transform we can guarantee that 
all the definition clauses for variable $x$ shall indeed contain either $x$ or $\overline x$, 
therefore by using our heuristics we will not overlook any gate.
On the other hand, in general it is possible that under this restriction  
we may miss some gate definitions.
%, as the following example shows.
%
%\begin{example}\label{ex:missed_gate}
%Consider CNF $(x+\overline y)_1(y+\overline z)_2(z + \overline x)_3$.
%Note that this CNF implies that all the variables must be equal to each other,
%i.e., $x=y=z$.
%None of the definitions for variable $x$ (and the same applies to $y$ and $z$), however,
%is implied by only clauses 1 and 3 (i.e., those containing $x$).
%\end{example}

As a further step, we relax our search from the 
``gate definitions'' to the ``dependent variables''.
%Instead we  make use of the notion of minimal unsatisfiable subsets (MUS) for
%obtaining a small candidate set for a gate definition.
To explain the approach we consider a general CNF 
\[
\phi_1 = (A_1+x) \ldots (A_m+x) (B_1+\overline x) \ldots (B_n+\overline x) C_1  \ldots C_k,
\]
where $A_i$ and $B_j$ ($i\in[1\ldots m]$ and $j\in[1\ldots n]$) are subclauses,
and $C_i$ ($i\in[1\ldots k]$) are clauses.
Now, instead of the unsatisfiability check for the formula in Theorem~\ref{th:general},
we propose to detect the unsatisfiability of the \emph{CNF} $A_1 \ldots A_m B_1\ldots B_n$.
The consequences of this are summarized by Theorem~\ref{th:sound}.
Please note that the notations introduced 
in this paragraph are re-used in Theorem~\ref{th:sound}.

\begin{theorem}\label{th:sound}
Assume that $A_1 \ldots A_m B_1\ldots B_n$ is unsatisfiable,
and we are given its unsatisfiable core (not necessarily minimal) to be 
$A_{i_1} \ldots A_{i_p}B_{j_1} \ldots B_{j_q}$. Further let 
\begin{eqnarray*}
\phi_{def} &=& (A_{i_1}+x) \ldots (A_{i_p}+x) (B_{j_1}+\overline x) \ldots (B_{j_q}+\overline x) \\
\phi_{rem} &=& C_1\ldots C_k \wedge (B_1+\overline x) \ldots (B_n+\overline x)\wedge\\
&\wedge &\bigwedge\nolimits_{t\in[1\ldots m]\setminus\{i_1,\ldots ,i_p\}} (A_t+x) \\
\phi_2 & = & (x \equiv \overline{\phi_{def}}|_{\overline{x}}) \wedge \phi_{rem}.
\end{eqnarray*}
Then we say that $x = \overline{\phi_{def}}|_{\overline{x}}$ is a 
\emph{pseudo} definition for $x$, 
and the formulas $\phi_1$ and $\phi_2$ are 
%\emph{equi\-satisfiable}. 
equivalent.
\end{theorem}
Note that in Theorem~\ref{th:sound} $\phi_{def}$ and $\phi_{rem}$ are not
disjoint, but both contain the clauses $(B_{j_1}+\overline x) \ldots (B_{j_q}+\overline x)$.
The assumption of the theorem requires that
$\phi_{def}|_{\overline{x}} \wedge \phi_{def}|_{x}$ is unsatisfiable
instead of $\phi_{def}|_{\overline{x}} \equiv \phi_{def}|_{x}$
as in Theorem~\ref{th:general}.
%
\begin{proof}
First observe that 
\[
(x \equiv \overline{\phi_{def}}|_{\overline{x}}) \leftrightarrow
((A_{i_1}+x)\ldots (A_{i_p}+x)(\overline x \vee (\overline{A}_{i_1}\ldots\overline{A}_{i_p})))
\]
%
Therefore $\phi_2 = \phi_1 \wedge (\overline x \vee (\overline{A}_{i_1}\ldots\overline{A}_{i_p}))$.
Clearly, under any assignment to the variables $\phi_2\rightarrow\phi_1$.
Now assume that under a given assignment to its variables $\phi_1$ is true.
We prove that $(\overline x \vee (\overline{A}_{i_1}\ldots\overline{A}_{i_p}))$ must be true 
by splitting on the assignment to $x$: 
\begin{itemize}
\item If $x=0$ then clearly $(\overline x \vee (\overline{A}_{i_1}\ldots\overline{A}_{i_p}))$ is true. 
\item If $x=1$ then $B_{j_1}\wedge\ldots \wedge B_{j_q}$ must be true
(in order to satisfy $\phi_1$). Taking in account unsatisfiability of
$A_{i_1}\ldots A_{i_p}B_{j_1}\ldots B_{j_q}$ we conclude that $A_{i_1}\wedge \ldots \wedge A_{i_p}$ is false,
and therefore $(\overline x \vee (\overline{A}_{i_1}\ldots\overline{A}_{i_p}))$ is again true.
\end{itemize}
Now since $(\overline x \vee (\overline{A}_{i_1}\ldots\overline{A}_{i_p}))$ is true and $\phi_1$ is true,
$\phi_2$ must be true as well.
\end{proof}

Intuitively gate definitions and pseudo definitions are very similar. 
Both could be added into the structure of the de-CNFized circuit.
The only difference is that after detection of a gate we remove all its 
defining clauses from the CNF, while for pseudo gates we only remove those
defining clauses \emph{containing positive literal $x$}.
Please also note that if a CNF was obtained from a circuit through 
the Tseitin transformation,
then there is a big chance that 
$B_{j_1}\wedge\ldots \wedge B_{j_q} = \overline{A}_{i_1}\vee \ldots \vee \overline{A}_{i_p}$, 
given that $A_{i_1} \ldots A_{i_p}B_{j_1} \ldots B_{j_q}$ is a 
\emph{minimal} unsatisfiable core (MUS). 
In this special case pseudo definition becomes a gate definition,
since $\phi_{def}|_{\overline{x}} \equiv \phi_{def}|_{x}$ is unsatisfiable,
and the clauses $(B_{j_1}+\overline x) \ldots (B_{j_q}+\overline x)$ can be removed from
$\phi_{rem}$.
%%cs Should we refer to the example?
%
For instance, this situation occurs for the definition of $d$ in Example~\ref{extract_example} as is shown below.
After simplifying $\phi_{CNF}$ in Example~\ref{extract_example} 
by unit propagation on $f$, and collecting all the clauses 
containing $d$, we obtain 
\[
\phi_{d} = (b+d+x)(b+\overline d+\overline x)(\overline b+d+\overline y)(\overline b+\overline d+y)(c + d).
\]
Further, $\phi_{d}|_d\wedge \phi_{d}|_{\overline{d}} = (b+x)(b+\overline x)(\overline b+\overline y)(\overline b+y)(c)$
is unsatisfiable, with a minimal core $(b+x)(b+\overline x)(\overline b+\overline y)(\overline b+y)$.
Following Theorem~\ref{th:sound} we assign 
\[
\phi_{def} = (b+d+x)(b+\overline d+\overline x)(\overline b+d+\overline y)(\overline b+\overline d+y),
\] 
and notice that $\overline{\phi_{def}}|_{\overline{d}} \equiv \phi_{def}|_d$, therefore 
concluding that $d = (b + \overline x)(\overline b + y)$ is a gate definition.
Consequently, \texttt{CnfToCircuit}$(\phi_{CNF})$ returns precisely the original circuit $\phi$. 

Note that the extraction of gate definitions as sketched in this section is not yet tailored
towards QBF solving. If gate detection for a variable $x$ is used for QBF solving, we need the
additional restriction that $x$ is an existential variable and does not depend on universal variables
that follow $x$ in the quantifier prefix~\cite{PS:2009}.
%(however the second situation can not occur in 2QBFs considered in this paper).
%Note that following Example~\ref{extract_example}, 
%$\Phi_{CNF} = \forall x y \exists a b c d f.
%\phi_{CNF}$. It still holds that QBF $\Phi = \forall x y \exists a
%b. \phi$ and $\Phi_{CNF}$ are satisfiability equivalent.

The last thing to mention is function \textit{BuildCircuit} in 
line 12 of the algorithm in Figure~\ref{alg:circuit}.
This function builds the final circuit from all the discovered gates.
Careful cycle breaking is applied in the places
where circular dependencies between variables occur (e.g., $a=b$ and $b=a$). 
It is also more desirable to use smaller unsatisfiable cores while searching for 
semantic definitions in order to reduce the support sets of the defined variables, 
which might decrease the chance of creating a circular dependency. 
Moreover, by extracting smaller (and more) definitions, we detect more structure
in the CNF compared to extracting large definitions at once.
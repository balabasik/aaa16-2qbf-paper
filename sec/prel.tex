In this work we shall use commonly accepted notations
from Boolean algebra and logic.
A \emph{Boolean variable} is interpreted over the binary domain $\{0, 1\}$.
A \emph{literal} is either a variable or its negation.
A \emph{clause} (resp. \emph{cube}) is a disjunction (resp. conjunction) of literals
(sometimes we might use set operations on clause/cube literals as well for convenience).
A Boolean formula in conjunctive normal form (CNF) is a conjunction of clauses.
A Boolean formula in disjunctive normal form (DNF) is a disjunction of cubes.
Both CNF and DNF might be a subject to set operations for convenience.
As a notational convention, we may also sometimes omit the
conjunction symbol $(\wedge)$, denote disjunction $(\vee)$ by the symbol
``$+$,'' and represent negation $(\neg)$ by an overline.

For a Boolean formula $\phi(x_1,..,x_i,..,x_n)$, we say its positive (resp. negative)
cofactor with respect to variable
$x_i$, denoted $\phi|_{x_i}$ (resp. $\phi|_{\overline{x}_i}$),
is the formula 
%\[
$
\phi(x_1,..,x_{i-1},1,x_{i+1},..,x_n)
$
%\]
(resp. $\phi(x_1,..,x_{i-1},0,x_{i+1},..,x_n)$).
The cofactor definition also extends to a cube of literals $\alpha = l_1 \wedge .. \wedge l_m$,
with the following recursive definition $\phi|_\alpha = (\phi|_{\alpha\setminus l_m})|_{l_m}$,
where $\phi|_{l_m}$ is a positive (resp. negative) cofactor with respect to variable
$\mathit{var}(l_m)$ if $l_m$ is a positive (resp. negative) literal.
We say that formula $\phi$ is satisfiable if there is an assignment to its variables
that evaluates $\phi$ to true. We say $\phi$ is unsatisfiable otherwise.
We define an \emph{unsatisfiable core} of an unsatisfiable CNF formula $\phi$ as an 
arbitrary unsatisfiable subset of clauses of $\phi$.

A \emph{two-quantified Boolean formula} (2QBF) $\Phi$ over
universal variables $X = \{x_1, \ldots, x_m\}$ and existential
variables $Y = \{y_1, \ldots, y_n\}$
in \emph{prenex form} is of the form $\Phi = \forall x_1 \cdots x_m \exists y_1 \cdots y_n. \phi$,
where the quantification part is called the
\emph{prefix}, denoted $\Phi_{\mathrm{pfx}}$, and $\phi$, a
quantifier-free formula in terms of variables $X$ and $Y$, is called
the \emph{matrix}, denoted $\Phi_{\mathrm{mtx}}$.
%Please notice that formulas of the form
%$\Phi' = \exists y_1 \cdots y_n \forall x_1 \cdots x_m . \phi$ are 2QBFs as well,
%we however limit ourselves to the prior case, as latter is
%symmetrical up to the negation operation.
We say that 2QBF $\Phi$ is false if there exists an assignment $\alpha_X$ to $X$ variables
(also referred to as the winning strategy or winning move of the universal player)
such that $\Phi_{\mathrm{mtx}}\mid_{\alpha_X}$ (which is a function of $Y$ variables) is unsatisfiable.
We say $\Phi$ is true otherwise (i.e., a winning strategy for the universal player does not exist).
In this work we shall study efficient ways of determining 
the values of 2QBF formulas represented in prenex form. 
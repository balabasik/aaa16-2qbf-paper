We implemented the de-CNFization algorithm from Section 5
(with both heuristics enabled) into a tool \textsc{Cnf2Blif}
(on top of the \textsc{Minisat} SAT solver~\cite{ES03}),
and patched the 2QBF solver embedded in the synthesis tool \textsc{ABC}~\cite{ABC} to
support cofactor sharing heuristics, proposed in
Section 4\footnote{\textsc{Cnf2Blif}, \textsc{ABC}, and other tools can be found in the supplementary materials.
\ifdefined\FINAL
here:\\
\url{https://www.dropbox.com/s/c29kj1a3w2kv0d0/tools_benchmarks_results_aaai16.zip?dl=0}
\fi
}.
We also used CEGAR QBF solvers \textsc{RareQS}~\cite{JanotaKMC12}
and \textsc{Qesto}~\cite{JanotaM15} for comparison.
All the experiments were performed on a Linux machine with Xeon 2.3 GHz CPU and 32Gb of RAM.
All the tools were limited by 4Gb memory limit and 1200 seconds time limit.

For experiments we used two sets of benchmarks: 2QBF track of
QBFEVAL'10~\cite{QBF} and FPGA mapping benchmarks from~\cite{MishchenkoBFG15}.
%
\subsection{\normalsize 2QBF track of QBFEVAL'10~\cite{QBF}.}
%
We used 200 
%%cs PCNF benchmarks
CNF based QBFs in prenex form
 from 2QBF track of QBFEVAL'10 QBF solving competitions.
Preprocessor \textsc{Bloqqer} was used to trim 135 benchmarks which can
be solved by pure preprocessing.
Both \textsc{RareQS} and \textsc{Qesto} ran on the preprocessed benchmarks.
%Prior work showed that \textsc{Qesto} outperformed other solvers.
On the other hand
we used \textsc{Cnf2Blif} to extract circuits from unpreprocessed CNF formulas,
and then ran 2QBF solver embedded into \textsc{ABC} under two settings:
without cofactor sharing (further referred to as \textsc{ABC-}) and
with cofactor sharing (further referred to as \textsc{ABC+}).
%%cs(
%%cs In 17 out of 65 benchmarks (\textit{sortnetsort} family)
%%cs we did not find any circuit structure, they therefore were just translated
%%cs to products of sums circuits. For completeness reasons for those
%%cs 17 benchmarks we also converted them to circuits \textit{after preprocessing}
%%cs (prefix ``Pre'' prior to \textsc{ABC} indicates that 17 benchmarks were preprocessed
%%cs before translating them into circuits, and then solved by \textsc{ABC}).
%%cs
%%cs Reformulation follows.
%%cs Did I understand this correctly??????
%%cs)
A third version, called \textsc{PreABC+} is similar to \textsc{ABC+},
with the only difference that the QBF preprocessor \textsc{Bloqqer}
is used for preprocessing QBFs where \textsc{Cnf2Blif} did not
find any circuit structure. \emph{After preprocessing} the resulting CNFs
are then just translated to products of sums circuits and then
solved by \textsc{ABC+}. (In our experiments, \textsc{Cnf2Blif} did not
find any circuit structure in 17 out of 65 benchmarks (\textit{sortnetsort} family).)

Results of this part of experiments are shown in Table~\ref{tbl:2qbf}.
A cactus plot of time performance is shown in Figure~\ref{fig:2qbf}.
Extraction time of \textsc{Cnf2Blif} was negligible compared to solving therefore we omitted it.
%
\begin{figure}[t!]
\centering
    \hspace*{-1.4mm}\includegraphics[width=1.03\columnwidth]{./fig/2qbf_plot1_new.pdf}
    \vspace{-7mm}
    \caption{Cactus plot of solvers performance on 2QBF benchmark set from QBFEVAL'10.\label{fig:2qbf}}
\end{figure}
%
\begin{table}[t]
\centering \caption{\small Statistics for 2QBF track from QBFEVAL'10.}
\vspace{0.7mm}
\label{tbl:2qbf}
\scriptsize
\begin{tabular}{|c||c|c|c|c|c|} \hline \hline
           & \textsc{RareQS} & \textsc{Qesto} & \textsc{ABC-} & \textsc{ABC+} & \textsc{PreABC+}\\
\hline \hline
Solved     & 50        & 55           & 48        & 61       &  62     \\
Time, s    & 20658     & 17842        & 20677     & 7748     &  4124   \\
Iterations & NA        & 7.26M        & 46.0K     & 368K     &  153K   \\
\hline \hline
\end{tabular}
\par\smallskip
\vspace{-4mm}
\end{table}

From Table~\ref{tbl:2qbf} and Figure~\ref{fig:2qbf} we see that the circuit based
2QBF solver \textsc{PreABC+} well outperforms existing CNF based solvers.
More specific details are discussed below.
On 17 benchmarks where no circuit structure was found \textsc{PreABC+} was on average
3 times slower compared to \textsc{Qesto}.
The remaining 48 benchmarks were found highly structural.
On these \textsc{ABC+} was on average 28 times faster than \textsc{Qesto}.
\textsc{ABC+} in comparison to \textsc{ABC-} was on average 30\% faster,
however if we only consider problems solved within more than 100 iterations
(or alternatively solved in about more than 1 second)
cofactor sharing gives about an order of magnitude speed up.
This phenomenon is well explained by the fact that
within a few first iterations cofactor sharing occurs rarely, while on the
larger scale AIG nodes from the new cofactors are found to be previously hashed practically all the time.
%
\subsection{\normalsize FPGA mapping benchmarks from~\cite{MishchenkoBFG15}.}
%
In this part of experiments we used 100 (50 SAT and 50 UNSAT) 2QBF benchmarks from an FPGA mapping
application~\cite{MishchenkoBFG15}. This experiment was designed
to test the reverse engineering ability of our tool \textsc{Cnf2Blif} in reconstructing the circuits from CNF.
Original AIG circuits have 54 and 66 primary inputs,
for UNSAT and SAT cases respectively, of which precisely 6 are existentially quantified
(i.e., the remaining variables are outermost universal variables). Each circuit consists of about 250 AIG nodes.
Circuits were transformed into 
%%cs PCNF not introduced in preliminaries ...
%%cs PCNF 2QBFs 
QCNFs
using \textsc{ABC}'s internal engine.
The resulting QCNFs on average have 93 variables and 240 clauses.

After applying \textsc{Cnf2Blif} only 2 extra (existentially quantified)
primary inputs were introduced to the reconstructed circuits.
This means that reconstruction was not identical with respect to the original circuit,
but very close.
This is due to the fact that our algorithm was not able to properly
choose the right definition in several cases where 2 or more definitions
were available (which is possible, e.g., in presence of XOR gates).
Statistically, reconstructed AIG circuits have 56 and 68 primary inputs,
for UNSAT and SAT cases respectively, and about 300 AIG nodes.
Most of the found gates were found semantically (in contrast to QBFEVAL'10,
where most of the found gates were either AND-like or XOR-like).
Most of those gates are ITE-like gates which are hard to
find by template matching. Our approach on the other hand had
no difficulties in retrieving those gate definitions.

For the solving part we used \textsc{ABC} to solve original problems
(denoted \textsc{ABC original}) and reconstructed circuits (denoted \textsc{ABC}).
%As all the problems were solved very quickly, sharing did not play any role,
%%cs Do we need the statement "and therefore we simply disabled it"?
%%cs If disabling sharing makes the runs really faster, we should leave the text as it is.
%%cs If there is no difference, we could remove this information which sounds a little bit strange.
%and therefore we simply disabled it.

The \textsc{Bloqqer} preprocessor was found to degrade the performance of CNF QBF solvers significantly
on this benchmark set (due to its eager variable elimination and expansion settings),
therefore we do not include it in 
%%cs the statistics below.
this set of experiments.
Solving statistics are shown in Table~\ref{tbl:fulldsd}.
As one can observe from Table~\ref{tbl:fulldsd}, after reverse engineering
the circuit solving time increased slightly, but did not suffer much.
In contrast, CNF QBF solvers required several orders of magnitude more iterations,
and significantly larger solving time.
%
\begin{table}[t]
\centering \caption{\small Statistics for FPGA mapping benchmark set.}
\vspace{0.8mm}
\label{tbl:fulldsd}
\scriptsize
\begin{tabular}{|c||c|c|c|c|} \hline \hline
           & \textsc{RareQS} & \textsc{Qesto} & \textsc{ABC} & \textsc{ABC original}\\
\hline \hline
Solved     & 22       & 44      & 100   & 100      \\
Time, s    & 96.9K    & 75.0K   & 100.2  & 64.3     \\
Iterations & NA       & 6.46M   & 1403  & 1241     \\
\hline \hline
\end{tabular}
\par\smallskip
\vspace{-4mm}
\end{table}
%
%\begin{figure*}[t!]
%\centering
%    \caption{Cactus plot of solvers performance on fulldsd06 benchmark set from~\cite{MishchenkoBFG15}.}
%    \vspace{2mm}
%    \includegraphics[width=0.7\textwidth]{./fig/fulldsd_plot1_new.pdf}
%    \label{fig:fulldsd}
%\end{figure*}
%

Both experiments, and specifically the cumulated number of iterations shown
in Tables~\ref{tbl:2qbf} and \ref{tbl:fulldsd} confirm our main conjecture:
in CEGAR-based 2QBF solving CNF representation is good for carrying out SAT queries,
but cofactoring should be done on circuit level instead.

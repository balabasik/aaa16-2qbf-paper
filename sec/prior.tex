In Figure~\ref{alg:cegar} we outline the generic CEGAR-based
algorithm \texttt{Cegar2Qbf} for solving an arbitrary 2QBF formula
in prenex form, introduced in \cite{JanotaKMC12}.
%
\begin{figure}[t]
\center{Cegar2Qbf}\\
\vspace{-2mm} \hrulefill
\begin{program}
\INPUT:  a QBF $\Phi = \forall X \exists Y . \phi$\\
\OUTPUT: True or False\\
\BEGIN\\
01 \> \> $\texttt{synMan}[X]:=1$; $\texttt{verMan}[X,Y]:=\phi$;\\
02 \> \> \WHILE True\\
03 \> \> \> $\alpha_X$ := SatSolve($\texttt{synMan}$);\\
04 \> \> \> \IF $\alpha_X=\emptyset$ \THEN \RETURN True;\\
05 \> \> \> $\alpha_Y$ := SatSolve($\texttt{verMan}$, $\alpha_X$);\\
06 \> \> \> \IF $\alpha_Y=\emptyset$ \THEN \RETURN False;\\
07 \> \> \> $\texttt{negCof}$ := $\neg \phi|_{\alpha_Y}$;\\
08 \> \> \> $\texttt{synMan}$ := $\texttt{synMan}\wedge\texttt{negCof}$;\\
\END
\end{program}
\vspace{-2mm} \hrulefill \vspace{-2.5mm} \caption{\small CEGAR
algorithm for generic 2QBF solving.} \label{alg:cegar}
\end{figure}
\renewcommand\setstretch{1}

%Algorithm \texttt{Cegar2Qbf} is very intuitive. 
In line 1 of Figure~\ref{alg:cegar} 
we initialize two SAT solving managers:
\emph{synthesis manager} \texttt{synMan} (i.e., trying to guess
the winning strategy of the universal player) and \emph{verification manager}
\texttt{verMan} (i.e., trying to verify if the guess was correct or
not). Initially \texttt{synMan} contains variables $X$ and \texttt{verMan}
contains variables $X$ and $Y$. In line 3 we search for a
candidate winning strategy. If all candidates have been blocked,
the 2QBF is determined to be true in line 4. In line 5 we search for a
counterexample to the candidate winning strategy, which renders
$\phi$ true (i.e., it disproves the candidate winning move
$\alpha_X$). Note that if CNF is used as an underlying 
data structure for \texttt{verMan}, then $\alpha_X$ can be 
easily passed to the SAT solver via its assumptions interface 
(which is commonly available in modern SAT solvers, 
e.g., in \textsc{Minisat}~\cite{ES03}).
If no counterexample is found,
then we conclude in line 6 that 2QBF is false. Otherwise we compute a
cofactor $\phi|_{\alpha_Y}$ in line 7, and block all the knowingly
wrong candidates $X'$, such that $\phi|_{\alpha_Y}(X')=1$, in line 8. 
%(in details we negate the cofactor and compute the conjunction with the
%synthesis manager \texttt{synMan}). 
Please note, that the
major difference of this algorithm compared to QDPLL-based
algorithms~\cite{GNT06,DepQBF} is that 
%is precisely located in lines 7-8 of \texttt{Cegar2Qbf}. 
in QDPLL \texttt{negCof} is simply
substituted with $\neg\alpha_X$, i.e., with intention to block the
failed candidate within \texttt{synMan}. The strength of 
\texttt{Cegar2Qbf} is in that besides $\alpha_X$ it potentially
blocks several other assignments.
%Algorithm \texttt{Cegar2Qbf} will be the key to 
%our discussions through this work. 

Algorithm \texttt{Cegar2Qbf}
may be applied to an arbitrary 2QBF in prenex form 
regardless of the representation of its matrix.
Some QBF solvers use And-Inverter graphs (AIGs) 
as an underlying matrix structure~\cite{PS:2009,MishchenkoBFG15}.
On the other hand, as CNF has proven to be an
efficient data structure for SAT solving 
(e.g., due to an efficient representation of learnt information 
in the form of learnt clauses~\cite{ES03}), 
(Q)CNF is also the most commonly accepted QBF matrix representation format~\cite{qdimacs}.
We therefore distinguish between \emph{CNF} and \emph{circuit} 2QBF solvers, 
depending on the matrix input format that they accept.

Up to the authors knowledge there is no much research done 
on the circuit CEGAR-based 2QBF solving besides that in~\cite{MishchenkoBFG15}.
On the other hand CNF CEGAR-based 2QBF solving was studied in~\cite{JanotaKMC12}
and~\cite{JanotaM15}.
Below we explain the efficient implementation idea of CNF CEGAR-based 2QBF solver, 
introduced in~\cite{JanotaM15} (we shall call it \textsc{Qesto}, by the name of the tool).

Please note that \texttt{verMan} is initialized to $\phi$ itself 
in line 1 of Figure~\ref{alg:cegar} and is never
changed, while \texttt{synMan} is constantly changed by conjunctions with
\texttt{negCof} in line 8.
%%cs I think there is no English word "to conjunct"
If the matrix is already represented in CNF, then the main
complication of the algorithm is the CNFization of \texttt{negCof}
prior to conjunction with \texttt{synMan}. 
The approach of~\cite{JanotaKMC12} suggested to use 
a Tseitin transform, 
i.e., to perform a syntactic
negation at the cost of introducing fresh variables, independently 
for each cofactor. This approach,
however, suffers from variable blow up within \texttt{synMan}
after a large number of iterations.
On the other hand the \textsc{Qesto} approach shall allow 
us to efficiently represent larger numbers of
cofactors within \texttt{synMan} without suffering from the variables blow up.
The idea is as follows: Consider a matrix $\phi = C_1 \wedge
C_2 \wedge .. \wedge C_n$, where each $C_i$ is split into
existential literals $C_{ei}$ and universal literals $C_{ui}$.
Notice that regardless of the specifics of the assignment $\alpha_Y$,
\texttt{negCof} always takes the form $\neg C_{uj_1} \vee \neg
C_{uj_2} \vee .. \vee \neg C_{uj_k}$. This observation suggests to
introduce definitions $d_i \equiv \neg C_{ui}$ for each $i\in[1..n]$,
which allows to represent \texttt{negCof} now conveniently as a
clause $(d_{j_1}+d_{j_2}+..+d_{j_k})$. This 
%%cs in its turn 
means that
we introduce at most $n$ fresh variables, independently from the number of
iterations of the while loop (line 2 in Figure~\ref{alg:cegar}). 
Under this scenario, the algorithm in
Figure~\ref{alg:cegar} is modified to initialize the synthesis
manager by
%
\[
\texttt{synMan}[X,D]:=\bigwedge_{i\in[1..n]} (d_i \equiv \neg C_{ui}).
\ifdefined\FINAL
\footnote{$d_i \equiv \neg C_{ui}$ can be viewed as a 
conjunction of two implications. Since (apart from this definition) variables $d_i$ only occur as positive literals in the formula
\texttt{synMan}, $d_i \equiv \neg C_{ui}$  can be replaced by one of the two implications, namely by $d_i \Rightarrow \neg C_{ui}$.
This modification corresponds to using the Plaisted/Greenbaum encoding \cite{PG:86} instead of Tseitin transformation \cite{Tse70} and 
is also used in~\cite{JanotaM15}.
}
\fi
\]

The \textsc{Qesto} approach was experimentally shown to be superior to others~\cite{JanotaM15}.
In the next section we will examine the advantages and 
disadvantages of CNF CEGAR-based 2QBF solving,
and introduce a heuristics, inspired by \textsc{Qesto}, 
to improve existing circuit CEGAR-based 2QBF algorithms.  
%
%\input{sec/mymini}
% 